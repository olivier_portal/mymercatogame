import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import WriteResult = admin.firestore.WriteResult;


admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://node-soccerteams.firebaseio.com'
});

const db = admin.firestore();


export const do_transfer_an_existing_player_inbetween_2_teams = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const {playerId} = req.body;
            const {teamDelId} = req.body;
            const {teamAddId} = req.body;
            console.log(playerId);

            const playerRefToTransfer: DocumentSnapshot = await db
                .collection('players')
                .doc(playerId)
                .get();
            console.log(playerRefToTransfer);

            const playerToDeleteInTeam: WriteResult = await db
                .collection('teams')
                .doc(teamDelId)
                .collection('players')
                .doc(playerRefToTransfer.id)
                .delete();
            console.log(playerToDeleteInTeam);

            const playerRefToAddInTeam: WriteResult = await db
                .collection('teams')
                .doc(teamAddId)
                .collection('players')
                .doc(playerRefToTransfer.id)
                .set({ref: 'players/' + playerRefToTransfer.id, uid: playerRefToTransfer.id});
            console.log(playerRefToAddInTeam);

            return res.send('ok');

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
