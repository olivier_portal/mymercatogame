import {PlayerModel} from './player.model';

export interface TeamModel {
    id?: string,
    championship: string,
    leaguePlace: number,
    name: string,
    shirtColor: string,
    players: PlayerModel[],
}
