import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import DocumentData = admin.firestore.DocumentData;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://node-soccerteams.firebaseio.com'
});

const db = admin.firestore();

export const get_one_player = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const {playerUid} = req.body;

            if (!playerUid) {
                return res.send('playerUid required');
            }
            const playerToFindSnap: DocumentSnapshot = await db
                .collection('players')
                .doc(playerUid)
                .get();
            const player: DocumentData | undefined = playerToFindSnap.data();
            console.log(player);
            if (!player) {
                return res.send(`player ${playerUid} does not exist`);
            }
            return res.send(player);

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
