import {PlayerModel} from './player.model';

export interface TeamModel {
    championship: string,
    leaguePlace: number,
    name: string,
    shirtColor: string,
    players: PlayerModel[],
}
