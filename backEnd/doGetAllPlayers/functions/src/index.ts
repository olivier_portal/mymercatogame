import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import {PlayerModel} from './models/player.model';
import QuerySnapshot = admin.firestore.QuerySnapshot;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://node-soccerteams.firebaseio.com'
});

const db = admin.firestore();

export const do_get_all_players = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const playersSnap: QuerySnapshot = await db
                .collection('teams')
                .get();
            const result: PlayerModel[] = [];
            playersSnap.forEach((playerSnap) => result.push(playerSnap.data() as PlayerModel));
            return res.send(result);

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
