import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import {TeamModel} from './models/team.model';
import DocumentSnapshot = admin.firestore.DocumentSnapshot;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://node-soccerteams.firebaseio.com'
});

const db = admin.firestore();

export const do_update_a_team = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const {teamId} = req.body;

            const newTeam: TeamModel = req.body;

            const teamRef = await db
                .collection('teams')
                .doc(teamId);
            const teamSnap: DocumentSnapshot = await teamRef.get();

            const teamToPatch:TeamModel = teamSnap.data() as TeamModel;
            if (!teamToPatch) {
                throw new Error('This team does not exist');
            }

            await teamRef.update(newTeam);

            return res.send('ok');

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
