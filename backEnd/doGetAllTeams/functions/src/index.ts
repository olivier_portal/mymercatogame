import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import {TeamModel} from './models/team.model';
import QuerySnapshot = admin.firestore.QuerySnapshot;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://node-soccerteams.firebaseio.com'
});

const db = admin.firestore();

export const do_get_all_teams = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const teamsSnap: QuerySnapshot = await db
                .collection('teams')
                .get();
            const result: TeamModel[] = [];
            teamsSnap.forEach((teamSnap) => result.push(teamSnap.data() as TeamModel));
            return res.send(result);

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
