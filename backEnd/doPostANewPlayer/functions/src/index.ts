import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import {PlayerModel} from './models/player.model';
import DocumentReference = admin.firestore.DocumentReference;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://node-soccerteams.firebaseio.com'
});

const db = admin.firestore();

export const do_post_a_new_player = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const newPlayer: PlayerModel = req.body;
            if(!newPlayer) {
                return res.send('Player must be filled');
            }
            const addResult: DocumentReference = await db
                .collection('players')
                .add(newPlayer);
            return res.send({...newPlayer, id: addResult.id});

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
