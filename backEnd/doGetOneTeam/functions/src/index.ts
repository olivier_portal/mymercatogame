import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import DocumentData = admin.firestore.DocumentData;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: "https://node-soccerteams.firebaseio.com"
});

const db = admin.firestore();

export const get_one_team = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const {teamUid} = req.body;

            if (!teamUid) {
                return res.send(`teamId required`);
            }
            const teamToFindSnap: DocumentSnapshot = await db
                .collection('teams')
                .doc(teamUid)
                .get();
            const team: DocumentData | undefined = teamToFindSnap.data();
            console.log(team);
            if (!team) {
                return res.send(`team ${teamUid} does not exist`);
            }
            return res.send(team);

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
