import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import DocumentSnapshot = admin.firestore.DocumentSnapshot;
import WriteResult = admin.firestore.WriteResult;


admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://node-soccerteams.firebaseio.com'
});

const db = admin.firestore();


export const do_delete_an_existing_player_in_team = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const {playerId} = req.body;
            const {teamId} = req.body;

            const playerRefToDelete: DocumentSnapshot = await db
                .collection('players')
                .doc(playerId)
                .get();
            console.log(playerRefToDelete);

            const playerToDeleteInTeam: WriteResult = await db
                .collection('teams')
                .doc(teamId)
                .collection('players')
                .doc(playerRefToDelete.id)
                .delete();
            console.log(playerToDeleteInTeam);

            return res.send('ok');

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
