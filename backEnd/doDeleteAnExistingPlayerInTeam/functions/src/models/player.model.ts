export interface PlayerModel {
    id: string,
    firstName: string,
    lastName: string,
    playerNumber: number,
    playerPosition: string,
    playerState: PlayerState,
}

export enum PlayerState {
    selected = 'titulaire',
    substitute = 'remplaçant',
    onTouch = 'sur le banc',
    injured = 'blessé',
}
