import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from './cert/cert';
import {TeamModel} from './models/team.model';
import DocumentReference = admin.firestore.DocumentReference;

admin.initializeApp({
    credential: admin.credential.cert(cert),
    databaseURL: 'https://node-soccerteams.firebaseio.com'
});

const db = admin.firestore();

export const do_post_a_new_team = functions
    .region('europe-west3')
    .runWith({timeoutSeconds: 15, memory: '256MB'})
    .https
    .onRequest(async (req: any, res: any) => {
        try {
            const newTeam: TeamModel = req.body;
            if(!newTeam) {
                return res.send('Team must be filled');
            }
            const addResult: DocumentReference = await db
                .collection('teams')
                .add(newTeam);
            return res.send({...newTeam, id: addResult.id});

        } catch (e) {
            console.error(e.message);
            return res.status(404);
        }

    });
