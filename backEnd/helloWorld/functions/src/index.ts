import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import {cert} from "./cert/cert";

admin.initializeApp({
 credential: admin.credential.cert(cert),
 databaseURL: "https://node-soccerteams.firebaseio.com"
});


export const helloWorld = functions
    .https
    .onRequest((request, response) => {
 response.send("Hello from Firebase!");
});


